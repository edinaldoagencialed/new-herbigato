<?php
// HTTP
define('HTTP_SERVER', 'http://local.herbicat.com.br/admin/');
define('HTTP_CATALOG', 'http://local.herbicat.com.br/');

// HTTPS
define('HTTPS_SERVER', 'http://local.herbicat.com.br/admin/');
define('HTTPS_CATALOG', 'http://local.herbicat.com.br/');

// DIR
define('DIR_APPLICATION', '/home/edinaldo/projetos/herbicat/admin/');
define('DIR_SYSTEM', '/home/edinaldo/projetos/herbicat/system/');
define('DIR_IMAGE', '/home/edinaldo/projetos/herbicat/image/');
define('DIR_STORAGE', '/home/edinaldo/projetos/herbicat/storage/');
define('DIR_CATALOG', '/home/edinaldo/projetos/herbicat/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'agencialed');
define('DB_DATABASE', 'herbicat');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
