<?php
class ControllerCustompagesProduct extends Controller
{
	public function index()
	{

		

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['search'] = $this->load->controller('common/search');



		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('<i class="fa fa-home" aria-hidden="true"></i>'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Pulverizadores',
			'href' => $this->url->link('custompages/pulverizadores')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'HERBICITRUS - PULVERIZADOR BARRA CITRUS DUPLA',
			'href' => $this->url->link('custompages/product')
		);

		$this->response->setOutput($this->load->view('custompages/product', $data));
	}
}
